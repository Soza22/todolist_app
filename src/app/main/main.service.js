(function() {
  'use strict';

  angular
  .module('angularSeedApp')
  .service('MainService', MainService);

  /** @ngInject */
  function MainService() {
   var data = [{
    "name": "Designing",
    "status": "Active",
    },{
    "name": "Coding",
    "status": "Active",
    },{
    "name": "Testing",
    "status": "Active",
    },{
    "name": "Code Review",
    "status": "Active"
    },{"name": "Deployment","status": "Active",}];

  if( !localStorage.getItem('taks')){
    localStorage.setItem('tasks', JSON.stringify(data));
  }
  
  return {
    getTasks: function() {

      return JSON.parse(localStorage.getItem("tasks"));
    },
    saveTask:function(tasks) {

      localStorage.setItem('tasks', JSON.stringify(tasks));

    }
  };
}
})();
